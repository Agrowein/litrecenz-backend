import {
  Body,
  Controller,
  HttpCode,
  Post,
  UnauthorizedException,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { SignInDto } from './dto/sign-in.dto';
import { SignUpDto } from './dto/sign-up.dto';
import { UserService } from '../../secured/user/user.service';
import { USER_ALREADY_EXIST, USER_NOT_FOUND } from '../../error/auth.errors';
import { genSalt, hash } from 'bcryptjs';
import { CreateUserDto } from '../../secured/user/dto/create-user.dto';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @UsePipes(new ValidationPipe())
  @HttpCode(200)
  @UseGuards(AuthGuard('local'))
  @Post('/sign-in')
  async signIn(@Body() { email, password }: SignInDto) {
    const user = await this.userService.findOne(email);
    // Если пользователь не найден - он не зарегистрирован
    if (!user) throw new UnauthorizedException(USER_NOT_FOUND);
    await this.authService.validateUser(user, password);
    return await this.authService.login(email);
  }

  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('local'))
  @Post('/sign-up')
  async signUp(@Body() dto: SignUpDto) {
    const user = await this.userService.findOne(dto.email);
    if (user) throw new UnauthorizedException(USER_ALREADY_EXIST);
    const salt = await genSalt(10);
    const newUser: CreateUserDto = {
      email: dto.email,
      passwordHash: await hash(dto.password, salt),
      name: dto.name,
      second_name: dto.second_name
    };

    return this.userService.create(newUser);
  }

}
