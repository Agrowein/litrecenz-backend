import {Injectable} from '@nestjs/common';
import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';

@Injectable()
export class PublicStrategy extends PassportStrategy(Strategy) {

}