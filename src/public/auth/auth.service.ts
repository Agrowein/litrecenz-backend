import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User } from '../../secured/user/entities/user.entity';
import { INCORRECT_PASSWORD } from '../../error/auth.errors';
import { compare } from 'bcryptjs';
import {JwtService} from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  async validateUser(user: User, password) {
    const isCorrectPassword = await compare(password, user.passwordHash);
    // Неверный пароль
    if (!isCorrectPassword) throw new UnauthorizedException(INCORRECT_PASSWORD);
  }

  async login(email: string) {
    const payload = { email };
    return {
      access_token: this.jwtService.sign(payload),
    }
  }
}
