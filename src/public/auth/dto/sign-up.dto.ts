import { CreateUserDto } from '../../../secured/user/dto/create-user.dto';
import { OmitType } from '@nestjs/mapped-types';
import { IsString, MaxLength, MinLength } from 'class-validator';

export class SignUpDto extends OmitType(CreateUserDto, ['passwordHash'] as const) {
  @MaxLength(200)
  @MinLength(6)
  @IsString()
  password: string;
}
