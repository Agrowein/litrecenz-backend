import {IsString, MaxLength, MinLength} from 'class-validator';

export class SignInDto {
  @MaxLength(200)
  @MinLength(10)
  @IsString()
  email: string;

  @MaxLength(100)
  @MinLength(6)
  @IsString()
  password: string;
}
