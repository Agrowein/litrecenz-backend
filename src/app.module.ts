import { Module } from '@nestjs/common';
import { PublicModule } from './public/public.module';
import { SecuredModule } from './secured/secured.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import ormConfig from './config/orm.config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      useFactory: ormConfig,
      imports: [ConfigModule],
      inject: [ConfigService],
    }),
    PublicModule,
    SecuredModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
