export const USER_NOT_FOUND = 'Пользователь с таким email не найден';
export const USER_ALREADY_EXIST = 'Пользователь с таким email уже зарегистрирован';
export const INCORRECT_PASSWORD = 'Неверный пароль';