import { IsString, MaxLength, MinLength } from 'class-validator';

export class CreateUserDto {
  @MaxLength(200)
  @IsString()
  email: string;

  @IsString()
  passwordHash: string;

  @MaxLength(200)
  @MinLength(6)
  @IsString()
  name: string;

  @MaxLength(200)
  @MinLength(6)
  @IsString()
  second_name: string;
}
